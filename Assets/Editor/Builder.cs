﻿using UnityEditor;

public static class Builder
{
    [MenuItem("File/Builder/Addressables")]
    public static void BuildAddressableContent()
    {
        UnityEditor.AddressableAssets.Settings.AddressableAssetSettings.BuildPlayerContent();
    }
}
