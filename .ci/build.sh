#!/usr/bin/env bash

set -e
set -x

echo "Building for $BUILD_TARGET"

UPPERCASE_BUILD_TARGET=${BUILD_TARGET^^};
if [ $UPPERCASE_BUILD_TARGET = "ANDROID" ]
then
  echo -e "Pkg.Desc = Android NDK\nPkg.Revision = 19.0.5232133" > $ANDROID_NDK_HOME/source.properties
fi

export BUILD_PATH=./Builds/$BUILD_TARGET/
mkdir -p $BUILD_PATH

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -projectPath $(pwd) \
  -quit \
  -batchmode \
  -buildTarget $BUILD_TARGET \
  -username "$UNITY_USERNAME" -password "$UNITY_PASSWORD" -serial "$UNITY_SERIAL" \
  -executeMethod $BUILD_CMD \
  -logFile /dev/stdout \
  -nographics \
  -CacheServerIPAddress $CACHE_SERVER 

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi
